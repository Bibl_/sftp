package sftp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server extends Thread {

	private final int port;
	private final List<Connection> clients;
	private volatile boolean running;
	
	public Server(int port) {
		this.port = port;
		clients = new ArrayList<Connection>();
	}
	
	@Override
	public synchronized void start() {
		running = true;
		super.start();
	}
	
	public synchronized void close() {
		running = false;
	}
	
	public synchronized boolean isRunning() {
		return running;
	}
	
	@Override
	public void run() {
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			while(running) {
				Socket clientSocket = serverSocket.accept();
				System.out.println("Server accepted client socket: " + clientSocket.hashCode());
				Connection client = new ServerToClientConnection(clientSocket);
				clients.add(client);
				client.start();
			}
		} catch (IOException e) {
			System.err.println("Server IO error: " + e.getMessage());
		} finally {
			System.out.println("Closing server.");
			running = false;
			for(Connection c : clients) {
				c.close();
			}
			if(serverSocket != null) {
				try {
					serverSocket.close();
				} catch (IOException e) {
					System.err.println("IO error closing server socket: " + e.getMessage());
				}
			}
		}
	}
}