package sftp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.util.Arrays;

import javax.crypto.SecretKey;

public abstract class Connection extends Thread {

	public final Socket socket;
	private final DataInputStream in;
	private final DataOutputStream out;
	public volatile boolean running;

	public BigInteger modulus, base, otherPublic, selfPrivate, selfPublic;
	public SecretKey secretKey;

	public Connection(Socket cSocket) throws IOException {
		this.socket = cSocket;
		in = new DataInputStream(socket.getInputStream());
		out = new DataOutputStream(socket.getOutputStream());
		running = true;
	}
	
	public InBuffer readPacket() throws IOException {
		System.out.printf("%s reading packet.%n", this.getClass().getName().substring(5, 11));
		DataInputStream _dis = new DataInputStream(in);
		int len = _dis.readInt();
		System.out.println("C.  Pre Length=" + len);
		byte[] payload = new byte[len];
		int read = _dis.read(payload);
		if(len != read) {
			throw new IOException(String.format("Only %d/%d bytes were read.", read, len));
		}
		if(isAuthenticated()) {
			payload = Utils.decrypt(payload, secretKey);
		}
		System.out.println("C.  Authed=" + isAuthenticated());
		System.out.println("C.  PostLength=" + payload.length);
		System.out.println("C.  Id=" + new InBuffer(payload).dis.readByte());
		System.out.println("S.  " + Arrays.toString(payload));
		
		return new InBuffer(payload);
	}
	
	public OutBuffer newBuffer() {
		return new OutBuffer();
	}
	
	public void send(OutBuffer buff) throws IOException {
		byte[] payload = buff.payload();
		
		System.out.printf("%s writing packet.%n", this.getClass().getName().substring(5, 11));

		System.out.println("S.  Id=" + new InBuffer(payload).dis.readByte());
		System.out.println("S.  Authed=" + isAuthenticated());

		System.out.println("S.  Pre Length=" + payload.length);
		System.out.println("S.  " + Arrays.toString(payload));
		
		if(isAuthenticated()) {
			payload = Utils.encrypt(payload, secretKey);
		}
	
		System.out.println("S.  PostLength=" + payload.length);
				
		out.writeInt(payload.length);
		out.write(payload);
	}
	
	public boolean isAuthenticated() {
		return secretKey != null;
	}

	@Override
	public void run() {
		try {
			while (running) {
				try {
					// handle0();
					if (!handle()) {
						close();
					}
				} catch (Exception e) {
					e.printStackTrace();
					break;
				}
			}
		} finally {
			close();
		}

		System.out.printf("Closed connection to %s:%d.%n", socket.getInetAddress().getHostAddress(), socket.getPort());
	}

	public abstract boolean handle() throws Exception;

	public void close() {
		if (running) {
			try {
				running = false;
				in.close();
				out.flush();
				out.close();
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static class OutBuffer {
		private final ByteArrayOutputStream baos;
		public final DataOutputStream dos;
		
		public OutBuffer() {
			baos = new ByteArrayOutputStream();
			dos = new DataOutputStream(baos);
		}
		
		public byte[] payload() {
			return baos.toByteArray();
		}
	}
	
	public static class InBuffer {
		public final DataInputStream dis;
		
		public InBuffer(byte[] decrypted) throws IOException {
			this.dis = new DataInputStream(new ByteArrayInputStream(decrypted));
		}
	}
}