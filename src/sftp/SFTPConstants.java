package sftp;

public interface SFTPConstants {
		
	int DEFAULT_PORT = 1993;
	
	int DIFFIE_PRIME_BIT_LENGTH = 1024;
	
	int OPCODE_AUTH_INIT = 1;
	int OPCODE_AUTH_RESP = 2;
	int OPCODE_AUTH_CLIENT_RESP = 3;
	
	int OPCODE_REQUEST = 1;
	int OPCODE_RESPONSE = 2;
	
	int TRANSFER_SUCCESS = 1;
	
}