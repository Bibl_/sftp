package sftp;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Utils {

	public static byte[] cipher(byte[] payload, int mode, Key key) {
		try {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(mode, key);
			return cipher.doFinal(payload);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
			return payload;
		}
	}

	public static byte[] encrypt(byte[] payload, Key key) {
		return cipher(payload, Cipher.ENCRYPT_MODE, key);
	}
	
	public static byte[] decrypt(byte[] payload, Key key) {
		return cipher(payload, Cipher.DECRYPT_MODE, key);
	}
	
	public static byte[] sha1(byte[] payload) throws NoSuchAlgorithmException {
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		sha.update(payload);
		return Arrays.copyOf(sha.digest(), 16); // trim key to 16 bytes (128 bit)
	}
}