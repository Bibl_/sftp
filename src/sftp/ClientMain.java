package sftp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

import sftp.ClientToServerConnection.Requester;
import sftp.Connection.InBuffer;
import sftp.Connection.OutBuffer;

public class ClientMain {

	public static void main(String[] args) throws IOException {
		int port = SFTPConstants.DEFAULT_PORT;
		String host = "localhost";
		if(args.length > 1) {
			try {
				host = args[0];
				port = Integer.parseInt(args[1]);
			} catch(NumberFormatException e) {
				System.err.printf("'%s' is not a valid port number.%n", args[0]);
			}
		}
		
		Scanner scanner = new Scanner(System.in);
		
		InetAddress addr = InetAddress.getByName(host);
		Socket socket = new Socket(addr, port);
		ClientToServerConnection.Requester req = new Requester() {			
			@Override
			public void handle(ClientToServerConnection con) {
				String line = scanner.nextLine();
				if(line != null && !line.isEmpty()) {
					String[] parts = line.split(" ");
					if(parts.length > 2 && parts[0].equals("request")) {
						try {
							OutBuffer out = con.newBuffer();
							out.dos.writeByte(SFTPConstants.OPCODE_REQUEST);
							out.dos.writeUTF(parts[1]);
							con.send(out);
							
							InBuffer in = con.readPacket();
							int opcode = in.dis.readByte();
							if(opcode != SFTPConstants.OPCODE_RESPONSE) {
								throw new UnsupportedOperationException();
							}
							int resp = in.dis.readByte();
							if(resp == SFTPConstants.TRANSFER_SUCCESS) {
								int len = in.dis.readInt();
								byte[] sdata = new byte[len];
								in.dis.read(sdata);
								String s = new String(sdata, "UTF-8");
								try(BufferedWriter bw = new BufferedWriter(new FileWriter(new File(parts[2])))) {
									bw.write(s);
								}
							} else {
								System.err.println("Couldn't retrieve file.");
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						System.err.println("Unrecognised command: " + line);
					}
				} else {
					System.err.println("Invalid command.");
				}
			}
		};
		ClientToServerConnection con = new ClientToServerConnection(socket, req);
		con.start();
	}
}