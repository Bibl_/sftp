package sftp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.spec.SecretKeySpec;

public class ServerToClientConnection extends Connection {
	
	public ServerToClientConnection(Socket cSocket) throws IOException {
		super(cSocket);
	}

	@Override
	public boolean handle() throws Exception {
		InBuffer in = readPacket();
		int opcode = in.dis.readByte();
		if(!isAuthenticated()) {
			switch(opcode) {
				case SFTPConstants.OPCODE_AUTH_INIT: {
					System.out.println("Server received auth_init.");
					SecureRandom rnd = new SecureRandom();
					modulus = BigInteger.probablePrime(SFTPConstants.DIFFIE_PRIME_BIT_LENGTH, rnd);
					base = BigInteger.probablePrime(SFTPConstants.DIFFIE_PRIME_BIT_LENGTH, rnd);
					selfPrivate = new BigInteger(Integer.toString(rnd.nextInt(Integer.MAX_VALUE - 1) + 1));
					selfPublic = base.modPow(selfPrivate, modulus);
					// A = g^a mod p

					System.out.println("Server sending auth_resp.");
					System.out.println("Server modulus:  " + modulus);
					System.out.println("Server base:     " + base);
					System.out.println("Server private:  " + selfPrivate);
					System.out.println("Server public:   " + selfPublic);
					
					OutBuffer out = newBuffer();
					out.dos.writeByte(SFTPConstants.OPCODE_AUTH_RESP);
					out.dos.writeUTF(modulus.toString());
					out.dos.writeUTF(base.toString());
					out.dos.writeUTF(selfPublic.toString());
					send(out);
					
					break;
				}
				case SFTPConstants.OPCODE_AUTH_CLIENT_RESP: {
					otherPublic = new BigInteger(in.dis.readUTF());
					BigInteger secretBigInt = otherPublic.modPow(selfPrivate, modulus);
					secretKey = new SecretKeySpec(Utils.sha1(secretBigInt.toByteArray()), "AES");

					System.out.println("Server received client_auth_init.");
					System.out.println("ServerOther public: " + otherPublic);
					System.out.println("Server Secret:      " + Arrays.toString(secretKey.getEncoded()));
				}
			}
		} else {
			switch(opcode) {
				case SFTPConstants.OPCODE_REQUEST: {
					String path = in.dis.readUTF();
					File f = new File(path);
					OutBuffer out = newBuffer();
					out.dos.writeByte(SFTPConstants.OPCODE_RESPONSE);
					
					if(f.exists()) {
						out.dos.writeByte(SFTPConstants.TRANSFER_SUCCESS); // success
						try(BufferedReader br = new BufferedReader(new FileReader(f))) {
							StringBuilder sb = new StringBuilder();
							String line;
							while((line = br.readLine()) != null) {
								sb.append(line).append('\n');
							}
							byte[] bytes = sb.toString().getBytes("UTF-8");
							out.dos.writeInt(bytes.length);
							out.dos.write(bytes);
						}
					} else {
						out.dos.writeByte(0); // failure
					}
					
					send(out);
				}
			}
		}
		return true;
	}
}