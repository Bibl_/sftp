package sftp;

import java.io.IOException;

public class ServerMain {

	public static void main(String[] args) throws IOException, InterruptedException {
		int port = SFTPConstants.DEFAULT_PORT;
		if(args.length > 0) {
			try {
				port = Integer.parseInt(args[0]);
			} catch(NumberFormatException e) {
				System.err.printf("'%s' is not a valid port number.%n", args[0]);
			}
		}
		
		Server server = new Server(port);
		server.start();
		System.out.println("Server started.");
	}
}