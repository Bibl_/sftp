package sftp;

import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.spec.SecretKeySpec;

public class ClientToServerConnection extends Connection {
	
	private final Requester requester;
	
	public ClientToServerConnection(Socket socket, Requester requester) throws IOException {
		super(socket);
		this.requester = requester;
	}
	
	@Override
	public boolean handle() throws Exception {
		if(!isAuthenticated()) {
			if(modulus == null || base == null || otherPublic == null) {
				System.out.println("Client sending auth_init.");
				
				OutBuffer out = newBuffer();
				out.dos.writeByte(SFTPConstants.OPCODE_AUTH_INIT); // 1
				send(out);
				
				InBuffer in = readPacket();
				int resp = in.dis.readByte(); // 2
				if(resp == SFTPConstants.OPCODE_AUTH_RESP) {
					modulus = new BigInteger(in.dis.readUTF());
					base = new BigInteger(in.dis.readUTF());
					otherPublic = new BigInteger(in.dis.readUTF());
					System.out.println("Client received auth_resp.");
					System.out.println("Client modulus:     " + modulus);
					System.out.println("Server base:        " + base);
					System.out.println("ServerOther public: " + otherPublic);
				} else {
					throw new IllegalStateException("Invalid auth resp response");
				}
			} else if(selfPrivate == null || selfPublic == null) {
				SecureRandom rnd = new SecureRandom();
				selfPrivate = new BigInteger(Integer.toString(rnd.nextInt(Integer.MAX_VALUE - 1) + 1));
				selfPublic = base.modPow(selfPrivate, modulus);

				System.out.println("Client sending auth_client_resp.");
				
				OutBuffer out = newBuffer();
				out.dos.writeByte(SFTPConstants.OPCODE_AUTH_CLIENT_RESP); // 3
				out.dos.writeUTF(selfPublic.toString());
				send(out);
				
				BigInteger secretBigInt = otherPublic.modPow(selfPrivate, modulus);
				secretKey = new SecretKeySpec(Utils.sha1(secretBigInt.toByteArray()), "AES");
				
				System.out.println("Client private: " + selfPrivate);
				System.out.println("Client public:  " + selfPublic);
				System.out.println("Client Secret:  " + Arrays.toString(secretKey.getEncoded()));
				System.out.println("Client SecretLen: " + secretKey.getEncoded().length);
			}
		} else {
			requester.handle(this);
		}
		return true;
	}
	
	static interface Requester {
		void handle(ClientToServerConnection con);
	}
}